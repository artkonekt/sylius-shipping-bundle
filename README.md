# UNDER DEVELOPMENT

# Integration into the app:

* [x] integrate the new shipping bundle into the application:
    * [x] register the bundle in AppKernel
    * [x] import the bundle's configuration in the app's configuration
    * [x] (generate and run migration)
    * [x] overwrite SyliusWebBundle/views/Backend/ShippingMethod/create.html.twig and SyliusWebBundle/views/Backend/ShippingMethod/update.html.twig: 
    they should include "ArtkonektSyliusShippingBundle:Backend/ShippingMethod:_form.html.twig" instead of the default one (this form contains the
    carrier field)
    
(TODO: convert this into a proper readme)