<?php
/**
 * Contains class CarrierChoiceType
 *
 * @package     Artkonekt\SyliusShippingBundle\Form\Type
 * @copyright   Copyright (c) 2016 Storm Storez Srl-D
 * @author      Lajos Fazakas <lajos@artkonekt.com>
 * @license     Proprietary
 * @since       2016-03-31
 * @version     2016-03-31
 */

namespace Artkonekt\SyliusShippingBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CarrierChoiceType extends AbstractType
{
    /**
     * Choices.
     *
     * @var array
     */
    protected $carriers;

    /**
     * Constructor.
     *
     * @param array $carriers
     */
    public function __construct(array $carriers)
    {
        $this->carriers = $carriers;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'choices' => $this->carriers,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'choice';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'artkonekt_sylius_shipping_carrier_choice';
    }
}