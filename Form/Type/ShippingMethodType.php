<?php
/**
 * Contains class ShippingMethodType
 *
 * @package     Artkonekt\SyliusShippingBundle\Form\Type
 * @copyright   Copyright (c) 2016 Storm Storez Srl-D
 * @author      Lajos Fazakas <lajos@artkonekt.com>
 * @license     Proprietary
 * @since       2016-03-31
 * @version     2016-03-31
 */

namespace Artkonekt\SyliusShippingBundle\Form\Type;

use Artkonekt\SyliusShippingBundle\Component\Core\Model\ShippingMethod;
use Sylius\Bundle\CoreBundle\Form\Type\ShippingMethodType as SyliusCoreShippingMethodType;
use Symfony\Component\Form\FormBuilderInterface;

class ShippingMethodType extends SyliusCoreShippingMethodType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->remove('categoryRequirement');

        $builder->add('categoryRequirement', 'choice', [
            'choices' => ShippingMethod::getCategoryRequirementLabels(),
            'multiple' => false,
            'expanded' => true,
            'label' => 'sylius.form.shipping_method.category_requirement',
        ]);

        $builder
            ->add('carrier', 'artkonekt_sylius_shipping_carrier_choice', [
                'label' => 'artkonekt_sylius_shipping.form.shipping_method.carrier',
                'required' => true
            ])
        ;
    }
}