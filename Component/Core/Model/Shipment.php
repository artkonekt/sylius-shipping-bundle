<?php
/**
 * Contains class Shipment
 *
 * @package     Artkonekt\SyliusShippingBundle\Component\Core\Model
 * @copyright   Copyright (c) 2016 Storm Storez Srl-D
 * @author      Lajos Fazakas <lajos@artkonekt.com>
 * @license     Proprietary
 * @since       2016-03-31
 * @version     2016-03-31
 */

namespace Artkonekt\SyliusShippingBundle\Component\Core\Model;

use Sylius\Component\Core\Model\Shipment as SyliusCoreShipment;

class Shipment extends SyliusCoreShipment implements CarrierAwareInterface, CarrierShipmentDetailsAwareInterface
{
    /**
     * @var string
     */
    protected $carrier;

    /**
     * @var array
     */
    protected $carrierShipmentDetails;

    /**
     * Returns the carrier.
     *
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Returns the carrier.
     *
     * @return string
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;
    }

    /**
     * Returns the carrier specific shipment details.
     *
     * @return array
     */
    public function getCarrierShipmentDetails()
    {
        return $this->carrierShipmentDetails;
    }

    /**
     * @param array $carrierDetails
     */
    public function setCarrierShipmentDetails($carrierDetails)
    {
        $this->carrierShipmentDetails = $carrierDetails;
    }
}