<?php
/**
 * Contains interface CarrierDetailsAwareInterface
 *
 * @package     Artkonekt\SyliusShippingBundle\Component\Core\Model
 * @copyright   Copyright (c) 2016 Storm Storez Srl-D
 * @author      Lajos Fazakas <lajos@artkonekt.com>
 * @license     Proprietary
 * @since       2016-03-31
 * @version     2016-03-31
 */

namespace Artkonekt\SyliusShippingBundle\Component\Core\Model;


interface CarrierShipmentDetailsAwareInterface
{
    /**
     * @return array
     */
    public function getCarrierShipmentDetails();
}