<?php
/**
 * Contains class ShippingMethod
 *
 * @package     Artkonekt\SyliusShippingBundle\Component\Core\Model
 * @copyright   Copyright (c) 2016 Storm Storez Srl-D
 * @author      Lajos Fazakas <lajos@artkonekt.com>
 * @license     Proprietary
 * @since       2016-03-31
 * @version     2016-03-31
 */

namespace Artkonekt\SyliusShippingBundle\Component\Core\Model;

use Sylius\Component\Core\Model\ShippingMethod as SyliusCoreShippingMethod;

class ShippingMethod extends SyliusCoreShippingMethod implements CarrierAwareInterface
{
    /**
     * @var string
     */
    protected $carrier;

    /**
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * @param string $carrier
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;
    }

    public static function getCategoryRequirementLabels()
    {
        $labels = parent::getCategoryRequirementLabels();
        $labels[ShippingMethodInterface::CATEGORY_REQUIREMENT_EXCEPT_ANY] = 'At least 1 unit has to NOT match the method category';

        return $labels;
    }
}