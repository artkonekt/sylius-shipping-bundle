<?php

/**
 * Contains class ShippingMethodInterface
 *
 * @package     Component\Shipping\Model
 * @copyright   Copyright (c) 2016 Storm Storez Srl-D
 * @author      Lajos Fazakas <lajos@artkonekt.com>
 * @license     Proprietary
 * @since       2016-08-17
 */


namespace Artkonekt\SyliusShippingBundle\Component\Core\Model;

use Sylius\Component\Shipping\Model\ShippingMethodInterface as SyliusShippingMethodInterface;

interface ShippingMethodInterface extends SyliusShippingMethodInterface
{
    const CATEGORY_REQUIREMENT_EXCEPT_ANY = 3;
}