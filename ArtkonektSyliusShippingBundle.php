<?php
/**
 * Contains class ArtkonektSyliusShippingBundle
 *
 * @package     Artkonekt\SyliusShipmentBundle
 * @copyright   Copyright (c) 2016 Storm Storez Srl-D
 * @author      Lajos Fazakas <lajos@artkonekt.com>
 * @license     Proprietary
 * @since       2016-03-31
 * @version     2016-03-31
 */

namespace Artkonekt\SyliusShippingBundle;


use Symfony\Component\HttpKernel\Bundle\Bundle;

class ArtkonektSyliusShippingBundle extends Bundle
{
    
}