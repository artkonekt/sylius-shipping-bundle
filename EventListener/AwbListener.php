<?php
/**
 * Contains class AwbListener
 *
 * @package     AppBundle\Courier
 * @copyright   Copyright (c) 2016 Storm Storez Srl-D
 * @author      Lajos Fazakas <lajos@artkonekt.com>
 * @license     Proprietary
 * @since       2016-03-02
 * @version     2016-03-02
 */

namespace Artkonekt\SyliusShippingBundle\EventListener;


use Konekt\CourierBundle\Event\AwbCreatedEvent;
use Konekt\CourierBundle\Event\AwbDeletedEvent;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ShipmentRepository;
use Doctrine\ORM\EntityManager;

class AwbListener
{
    private $em;
    private $shipmentRepository;

    /**
     * AwbListener constructor.
     *
     * @param ShipmentRepository $shipmentRepository
     * @param EntityManager      $em
     */
    public function __construct(ShipmentRepository $shipmentRepository, EntityManager $em)
    {
        $this->em = $em;
        $this->shipmentRepository = $shipmentRepository;
    }

    /**
     * Updates the tracking number of the shipment based on the AWB creation event.
     *
     * @param AwbCreatedEvent $event
     */
    public function updateShippingTrackingNumber(AwbCreatedEvent $event)
    {
        $shipment = $this->shipmentRepository->find($event->getPackageId());

        $shipment->setTracking($event->getAwbNumber());

        $this->em->persist($shipment);
        $this->em->flush();
    }

    public function deleteShippingTrackingNumber(AwbDeletedEvent $event)
    {
        $shipment = $this->shipmentRepository->findOneBy(['tracking' => $event->getAwbNumber()]);

        if ($shipment) {
            $shipment->setTracking(null);

            $this->em->persist($shipment);
            $this->em->flush();
        }
    }
}