<?php
/**
 * Contains class CheckoutListener
 *
 * @package     Artkonekt\SyliusShippingBundle\EventListener
 * @copyright   Copyright (c) 2016 Storm Storez Srl-D
 * @author      Lajos Fazakas <lajos@artkonekt.com>
 * @license     Proprietary
 * @since       2016-03-31
 * @version     2016-03-31
 */

namespace Artkonekt\SyliusShippingBundle\EventListener;


use Artkonekt\SyliusShippingBundle\Component\Core\Model\Shipment;
use Sylius\Component\Core\Model\OrderInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Doctrine\ORM\EntityManager;

class CheckoutListener
{
    /**
     * Sets the carrier on the shipment based on its shipping method. This is necessary because the carrier of the shipment
     * method can be changed later.
     *
     * @param GenericEvent $event
     */
    public function setCarrierOnShipment(GenericEvent $event)
    {
        /** @var OrderInterface $order */
        $order = $event->getSubject();

        /** @var Shipment $lastShipmentAdded */
        $shipments = $order->getShipments();

        //we don't use the $order->getLastShipment(), that works for already flushed shipments, at this stage the last
        // shipment is not yet flushed, only added to the collection
        $lastShipment = $shipments->last();
        $lastShipment->setCarrier($lastShipment->getMethod()->getCarrier());
    }
}